<?php

namespace App\Http\Controllers;

use App\ProductImage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class FileController extends Controller
{
    public function serveProductImage($file, Request $r) {
        preg_match_all('/(\d+).*/m', $file, $matches, PREG_SET_ORDER);
        if (!isset($matches[0][1])) {
            abort(404);
        }

        $id = intval($matches[0][1]);
        $image = ProductImage::findOrFail($id);
        $response = Storage::disk('s3')->response($image->filepath);
        $response->headers->set('Cache-Control', 'public, max-age=31536000');
        return $response;
    }
}
