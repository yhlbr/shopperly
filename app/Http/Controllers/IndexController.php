<?php

namespace App\Http\Controllers;

use App\Product;

class IndexController extends Controller
{
    public function index()
    {
        $products = Product::with('images')->orderBy('updated_at')->take(10)->get();

        return view('pages.index', ['products' => $products]);
    }
}
