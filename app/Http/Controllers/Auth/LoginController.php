<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers {
        logout as logoutUser;
    }

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/account/';

    protected $persistentSessionKeys = ['cart'];

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    public function logout(Request $request) {
        $sessionData = [];
        // Save persistent session data
        foreach ($this->persistentSessionKeys as $key) {
            $sessionData[$key] = $request->session()->get($key, '');
        }

        // Logging user out
        $response = $this->logoutUser($request);

        // Save persistent session data
        foreach ($this->persistentSessionKeys as $key) {
            $request->session()->put($key, $sessionData[$key]);
        }

        if (!$request->wantsJson()) {
            return redirect()->route('login')->with('success', __('user.logoutSuccess'));
        }

        return $response;
    }
}
