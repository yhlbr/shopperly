<?php

namespace App\Http\Controllers;

use App\Category;
use App\Locale;

class CategoryController extends Controller {
    public function index($category_link) {
        $category = Category::where(['seo_name' => $category_link])->firstOrFail();
        $products = $category->products()->skip(0)->take(10)->get();
        return view('pages.category', ['products' => $products]);
    }
}
