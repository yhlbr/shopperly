<?php

namespace App\Http\Controllers;

use App\Page;

class PageController extends Controller
{
    public function index($id) {
        $page = Page::findOrFail($id);
        return view('pages.page', ['page' => $page]);
    }

    public function styles($id) {
        $page = Page::findOrFail($id);
        $response = response()->make($page->{'gjs-css'});
        $response->header('Content-Type', 'text/css');
        return $response;
    }
}
