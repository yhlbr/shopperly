<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AccountController extends Controller
{
    public function index(Request $r) {
        $user = Auth::user();
        $address = OrderController::getCurrentAddress();
        $addAddressRoute = route('account.address.add');
        return view('pages.account.overview', compact('user', 'address', 'addAddressRoute'));
    }

    public function edit() {
        // TODO: implement
    }

    public function editAddress($id, Request $r) {
        // TODO: implement
    }

    public function addAddress() {
        $addressCount = Auth::user()->addresses->count();
        // TODO: implement saving
        return view('pages.account.addAddress', compact("addressCount"));
    }

    public function password() {
        return view('pages.account.password');
    }

    public function updatePassword(Request $r) {
        /* @var User $user */
        $user = Auth::user();

        $validationParams = [
            'password' => 'required|confirmed',
        ];
        if ($user->hasPassword()) {
            $validationParams['current_password'] = 'required';
        }

        $requestData = $r->validate($validationParams);
        if ($user->hasPassword() && !Hash::check($requestData['current_password'], $user->password)) {
            return back()->withErrors("Current Password doesn't match.");
        }
        $user->password = Hash::make($requestData['password']);
        $user->save();

        return redirect()->route('account.index')->with('success', __('user.savedPassword'));
    }
}
