<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;

class CartController extends Controller
{
    public function add($id, Request $r)
    {
        $cart = $r->session()->get('cart.products');
        if (!$cart) $cart = [];

        $productAlreadyInCart = false;
        foreach ($cart as $key => $product) {
            if ($product['id'] == $id) {
                $cart[$key]['count']++;
                $r->session()->put('cart.products', $cart);
                $productAlreadyInCart = true;
                break;
            }
        }

        if (!$productAlreadyInCart)
            $r->session()->push('cart.products', ['id' => intval($id), 'count' => 1]);
        return redirect()->route('cart');
    }

    public function cart(Request $r)
    {
        $cart = self::getCart($r);
        $total = 0;
        foreach ($cart as $product) {
            $total += $product['product']->price * $product['count'];
        }
        return view('pages.cart', ['cart' => $cart, 'total' => $total]);
    }

    public function modify($index, $amount, Request $r)
    {
        $key = 'cart.products.' . $index . '.count';
        if ($r->session()->has($key)) {
            if ($amount == 0) {
                $r->session()->forget('cart.products.' . $index);
            } else {
                $r->session()->put($key, intval($amount));
            }
        }
    }

    public static function getCart(Request $r)
    {
        $products = $r->session()->get('cart.products');
        if (!$products)
            $products = [];


        $cart = [];
        foreach ($products as $id => $p) {
            $product = Product::find($p['id']);

            if (!$product) {
                $r->session()->forget('cart.products.' . $id);
                continue;
            }

            $cart[$id] = ['product' => $product, 'count' => $p['count']];
        }
        return collect($cart);
    }
}
