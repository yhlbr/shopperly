<?php

namespace App\Http\Controllers;

use App\Classes\Stripe;
use App\Classes\Order;
use App\ShippingOption;
use App\UserAddress;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{
    public static function getCurrentAddress() {
        $addressId = request()->session()->get('order.addressId');
        if (!is_int($addressId)) {
            return Auth::user()->addresses()->first();
        }
        else {
            return UserAddress::find($addressId);
        }
    }

    public function checkout(Request $r)
    {
        if (!Auth::check()) {
            $r->session()->put('url.intended', url()->current());
            return redirect()->route('login');
        }

        $addressId = $r->session()->get('order.addressId');
        if (!is_int($addressId)) $addressId = -1;

        if ($addressId > 0) {
            $address = UserAddress::find($r->session()->get('order.addressId'));
            if ($address->user_id != Auth::id()) {
                $address = Auth::user()->addresses()->first();
                $r->session()->put('order.addressId', $address->id);
            }
        } elseif ($addressId == -1) {
            $address = Auth::user()->addresses()->first();
            if ($address) {
                $r->session()->put('order.addressId', $address->id);
            }
        } elseif ($addressId == 0) {
            $address = new UserAddress();
        }

        if (!$address) {
            $address = new UserAddress();
        }

        return view('pages.order.checkout', ['address' => $address, 'addressId' => $addressId]);
    }

    public function saveCheckout(Request $r)
    {
        $data = $r->validate([
            'firstname' => 'required|string|max:255',
            'lastname' => 'required|string|max:255',
            'company' => 'string|nullable|max:255',
            'street' => 'required|string|max:255',
            'street_no' => 'required|string|max:255',
            'city' => 'required|string|max:255',
            'postcode' => 'required|integer|digits_between:1,10',
            'country' => 'required|string|max:255',
            'address_name' => 'string|nullable|max:255'
        ]);

        $addressId = $r->session()->get('order.addressId');
        if (!is_int($addressId)) $addressId = -1;

        // If address was chosen, update the values
        if ($addressId > 0) {
            $address = UserAddress::find($r->session()->get('order.addressId'));
            if ($address->user_id == Auth::id()) {
                $address->update($data);
                $address = UserAddress::find($r->session()->get('order.addressId'));
            } else {
                $address = $this->saveAddress($data);
            }
        } else {
            $address = $this->saveAddress($data);
        }
        $r->session()->put('order.addressId', $address->id);
        return redirect()->route('order.shipping');
    }

    protected function saveAddress($data)
    {
        $data['user_id'] = Auth::id();
        if (!isset($data['address_name'])) $data['address_name'] = __('common.default');

        $address = UserAddress::create($data);
        return $address;
    }

    public function switchAddress($address_id, Request $r)
    {
        if ($address_id > 0) {
            $address = UserAddress::find($address_id);
            if ($address->user_id != Auth::id()) {
                return redirect()->back();
            }
        }

        $r->session()->put('order.addressId', intval($address_id));
        return redirect()->back();
    }

    public function shipping(Request $r)
    {
        $address = UserAddress::find(intval($r->session()->get('order.addressId')));
        $user = Auth::user();
        // Sanity-Checks
        if (!$address || $user->id != $address->user_id) {
            return redirect()->route('order.checkout');
        }

        $options = ShippingOption::get();
        return view('pages.order.shipping', ['address' => $address, 'shippingOptions' => $options]);
    }

    public function saveShipping($option_id, Request $r)
    {
        $option = ShippingOption::find($option_id);
        if (!$option) {
            return redirect()->route('order.shipping');
        }

        $r->session()->put('order.shippingId', intval($option_id));

        return redirect()->route('order.confirmation');
    }

    public function confirmation(Request $r)
    {
        $cart = CartController::getCart($r);
        if ($cart->isEmpty()) {
            return redirect()->route('cart');
        }

        $address = UserAddress::find(intval($r->session()->get('order.addressId')));
        $user = Auth::user();
        // Sanity-Checks
        if (!$address || $user->id != $address->user_id) {
            return redirect()->route('order.checkout');
        }

        $shipping = ShippingOption::find(intval($r->session()->get('order.shippingId')));
        // Sanity-Checks
        if (!$shipping) {
            return redirect()->route('order.shipping');
        }

        $total = Order::calculateTotalCost($cart, $shipping);

        $order = Order::getCurrentOrder($r, $cart);
        $intent = Stripe::instance()->getCurrentIntent($order, $r->session());

        return view('pages.order.confirmation', compact('cart', 'address', 'user', 'shipping', 'total', 'intent'));
    }

    public function complete(Request $r)
    {
        if (!$r->has('intentId'))
            return redirect()->back()->with('error', __('order.payment.validationError'));
        $intentId = $r->input('intentId');

        $order = Order::getCurrentOrder($r);
        $stripe = Stripe::instance();
        $intent = $stripe->getIntent($intentId);
        if (!$stripe->validateIntent($intent, $order)) {
            return redirect()->back()->with('error', __('order.payment.validationError'));
        }

        $order->intent_id = $intentId;
        $order->save();

        $r->session()->forget('cart.products');
        $r->session()->forget('order.intentId');

        return redirect()->route('order.success');
    }

    public function success()
    {
        return view('pages.order.success');
    }
}
