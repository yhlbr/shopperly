<?php

namespace App\Http\Controllers;

use App\User;
use App\UserOauth;
use Socialite;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class OauthController extends Controller
{
    public function redirect($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    public function callback($provider)
    {
        $user = Socialite::driver($provider)->user();
        if (Auth::user()) {
            $existingId = UserOauth::getUserIdFromOauth($provider, $user->getId());

            // If user not yet found, connect to existing user
            if ($existingId === false) {
                // Connect to logged in user
                UserOauth::addOauthToUser(Auth::id(), $provider, $user->getId());
            }
            // If user found, login to other user
            else {
                $newUser = User::find($existingId);
                Auth::logout();
                Auth::login($newUser, true);
            }
            return redirect()->route('index');
        }
        // Not logged in
        else {
            $existingId = UserOauth::getUserIdFromOauth($provider, $user->getId());

            // If user not yet found, create new user
            if ($existingId === false) {
                // TODO: Check if E-Mail already in use
                // Create User with data
                $newUser = User::create([
                    'email' => $user->getEmail(),
                    'email_verified_at' => Carbon::now()
                ]);
                UserOauth::addOauthToUser($newUser->id, $provider, $user->getId());

                Auth::login($newUser, true);
                return redirect()->route('account.password');
            }
            // if user found, login
            else {
                $newUser = User::find($existingId);
                Auth::login($newUser, true);
                return redirect()->route('index');
            }
        }
    }
}
