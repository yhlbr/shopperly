<?php

namespace App\Http\Controllers;

use App\Product;
use App\Locale;

class ProductController extends Controller
{
    public function index($seo_name)
    {
        $product = Product::where(['seo_name' => $seo_name])->with('images')->first();
        if (!$product) return redirect('index');
        return view('pages.product', ['product' => $product]);
    }
}
