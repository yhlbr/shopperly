<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Controllers\Controller;
use App\Translations\CategoryLocale;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function editIndex(Request $r)
    {
        return view('admin.pages.category.edit.index');
    }

    public function add()
    {
        return view('admin.pages.category.add.index', [
            'formAction' => route('admin.category.add.save')
        ]);
    }

    public function validateCategory(Request $r)
    {
        $values = $r->validate([
            'category_nr' => 'required|max:255',
            'visible' => '',
            'locales.*.name' => 'string|nullable|max:255',
            'locales.*.description' => 'string|nullable',
        ]);
        $values['visible'] = array_key_exists('visible', $values);
        return $values;
    }

    public function addSave(Request $r)
    {
        $values = $this->validateCategory($r);
        $values['seo_name'] = str_slug($values['name'], '-');

        cache()->forget('categoryTree');

        $locales = $values['locales'];
        unset($values['locales']);
        $category = Category::create($values);
        if (!$category)
            return redirect(route('admin.category.add'))
                ->with('alert-title', __('admin.category.titles.error'))
                ->with('alert-message', __('admin.category.messages.saveError'))
                ->with('alert-class', 'uk-alert-danger');

        foreach ($locales as $locale => $values) {
            $values['category_id'] = $category->id;
            $values['locale'] = $locale;
            CategoryLocale::create($values);
        }

        return redirect(route('admin.category.edit.index'))
            ->with('alert-title', __('admin.category.titles.saved'))
            ->with('alert-message', __('admin.category.messages.saved'))
            ->with('alert-class', 'uk-alert-success')
            ->with(
                'alert-html',
                '<a href="' . route('admin.category.images.show', ['category_id' => $category->id]) . '">'
                    . __('admin.category.messages.editImages') . '</a><br>' .
                    '<a href="' . route('admin.category.assign.show', ['category_id' => $category->id]) . '">'
                    . __('admin.category.messages.assignCategories') . '</a>'
            );
    }

    public function edit($id)
    {
        $category = Category::find($id);

        if (!$category) {
            return redirect()->route('admin.category.edit.index');
        }

        return view('admin.pages.category.edit.edit', [
            'category' => $category,
            'formAction' => route('admin.category.edit.save', ['category_id' => $id])
        ]);
    }

    public function editSave(Request $request, $id)
    {
        $values = $this->validateCategory($request);
        $locales = $values['locales'];
        unset($values['locales']);

        cache()->forget('categoryTree');

        $category = Category::find($id);
        if (!$category)
            return redirect(route('admin.category.edit', ['category_id' => $id]))
                ->with('alert-title', __('admin.category.titles.error'))
                ->with('alert-message', __('admin.category.messages.notFoundError'))
                ->with('alert-class', 'uk-alert-danger');

        $category->update($values);
        $category->translations()->delete();

        foreach ($locales as $locale => $values) {
            $values['category_id'] = $id;
            $values['locale'] = $locale;
            CategoryLocale::create($values);
        }

        return redirect(route('admin.category.edit.index'))
            ->with('alert-title', __('admin.category.titles.saved'))
            ->with('alert-message', __('admin.category.messages.saved'))
            ->with('alert-class', 'uk-alert-success');
    }

}
