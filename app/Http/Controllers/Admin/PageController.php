<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Locale;
use App\Http\Controllers\Controller;
use App\Page;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class PageController extends Controller {
    public function editIndex($id, Request $r) {

    }

    public function add() {
        return view('admin.pages.page.add');
    }

    public function edit($id) {
        return view('admin.pages.page.edit', ['id' => $id]);
    }

    public function editSave(Request $request, $id) {
        return redirect(route('admin.category.edit.index'))
            ->with('alert-title', __('admin.category.titles.saved'))
            ->with('alert-message', __('admin.category.messages.saved'))
            ->with('alert-class', 'uk-alert-success');
    }

    public function loadEditor($id) {
        return Page::find($id)->toJson();
    }

    public function saveEditor($id, Request $r) {
        $values = $r->all([
            'gjs-html',
            'gjs-components',
            'gjs-assets',
            'gjs-css',
            'gjs-styles',
        ]);

        // Assets are loaded directly from s3
        $values['gjs-assets'] = '[]';

        $page = Page::find($id);
        if ($page) {
            $page->update($values);
        }
    }

    public function assets() {
        $files = collect(Storage::disk('s3')->allFiles('assets/'))->map(function($file) {
            return Storage::disk('s3')->url($file);
        });
        return response()->json($files);
    }

    public function uploadAssets(Request $r) {
        $files = $r->allFiles();
        foreach ($files as $file) {
            $file->store('assets', 's3');
        }
    }
}
