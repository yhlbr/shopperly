<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\CategoryProduct;
use App\Product;
use App\Http\Controllers\Controller;
use App\ProductImage;
use App\Translations\ProductLocale;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class ProductController extends Controller
{
    public function editIndex(Request $r)
    {
        return view('admin.pages.product.edit.index', ['chooser' => self::showChooser($r, 'admin.product.edit')]);
    }

    public function imagesIndex(Request $r)
    {
        return view('admin.pages.product.images.index', ['chooser' => self::showChooser($r, 'admin.product.images.show')]);
    }

    public function assignIndex(Request $r)
    {
        return view('admin.pages.product.assign.index', ['chooser' => self::showChooser($r, 'admin.product.assign.show')]);
    }

    public function add()
    {
        return view('admin.pages.product.add.index', [
            'formAction' => route('admin.product.add.save')
        ]);
    }

    public function validateProduct(Request $r)
    {
        $values = $r->validate([
            'product_nr' => 'required|max:255',
            'price' => 'required|numeric',
            'weight' => 'required|numeric',
            'visible' => '',
            'locales.*.name' => 'string|nullable|max:255',
            'locales.*.description' => 'string|nullable',
        ]);
        $values['visible'] = array_key_exists('visible', $values);
        return $values;
    }

    public function addSave(Request $r)
    {
        $values = $this->validateProduct($r);
        $values['seo_name'] = Str::slug($values['locales']['en']['name'], '-');

        $locales = $values['locales'];
        unset($values['locales']);
        $product = Product::create($values);
        if (!$product)
            return redirect(route('admin.product.add'))
                ->with('alert-title', __('admin.product.titles.error'))
                ->with('alert-message', __('admin.product.messages.saveError'))
                ->with('alert-class', 'uk-alert-danger');

        foreach ($locales as $locale => $values) {
            $values['product_id'] = $product->id;
            $values['locale'] = $locale;
            ProductLocale::create($values);
        }

        return redirect(route('admin.product.edit.index'))
            ->with('alert-title', __('admin.product.titles.saved'))
            ->with('alert-message', __('admin.product.messages.saved'))
            ->with('alert-class', 'uk-alert-success')
            ->with(
                'alert-html',
                '<a href="' . route('admin.product.images.show', ['product_id' => $product->id]) . '">'
                    . __('admin.product.messages.editImages') . '</a><br>' .
                    '<a href="' . route('admin.product.assign.show', ['product_id' => $product->id]) . '">'
                    . __('admin.product.messages.assignCategories') . '</a>'
            );
    }

    public function edit($id)
    {
        $product = Product::find($id);
        if (!$product) {
            return redirect(route('admin.product.edit.index'));
        }

        return view('admin.pages.product.edit.edit', [
            'product' => $product,
            'formAction' => route('admin.product.edit.save', ['product_id' => $id])
        ]);
    }

    public function editSave(Request $request, $id)
    {
        $values = $this->validateProduct($request);
        $locales = $values['locales'];
        unset($values['locales']);

        $product = Product::find($id);
        if (!$product)
            return redirect(route('admin.product.edit', ['product_id' => $id]))
                ->with('alert-title', __('admin.product.titles.error'))
                ->with('alert-message', __('admin.product.messages.notFoundError'))
                ->with('alert-class', 'uk-alert-danger');

        $product->update($values);
        $product->translations()->delete();

        foreach ($locales as $locale => $values) {
            $values['product_id'] = $id;
            $values['locale'] = $locale;
            ProductLocale::create($values);
        }

        return redirect(route('admin.product.edit.index'))
            ->with('alert-title', __('admin.product.titles.saved'))
            ->with('alert-message', __('admin.product.messages.saved'))
            ->with('alert-class', 'uk-alert-success')
            ->with(
                'alert-html',
                '<a href="' . route('admin.product.images.show', ['product_id' => $id]) . '">'
                    . __('admin.product.messages.editImages') . '</a><br>' .
                    '<a href="' . route('admin.product.assign.show', ['product_id' => $id]) . '">'
                    . __('admin.product.messages.assignCategories') . '</a>'
            );
    }

    public function images($id)
    {
        $product = Product::find($id);
        if (!$product) return redirect()->route('admin.product.images.index');
        return view('admin.pages.product.images.images', ['product' => $product]);
    }

    public function imagesUpload(Request $req, $id)
    {
        if ($req->hasFile('files')) {
            $files = $req->file('files');
            foreach ($files as $file) {
                $path = $file->store('images', 's3');
                $filename = substr($path, strlen('images/'));
                $image = new ProductImage();
                $image->filename = $filename;
                $image->product_id = $id;
                $image->save();
            }
        }
    }

    public function deleteImage($image)
    {
        if (Storage::disk('s3')->exists('images/' . $image)) {
            Storage::disk('s3')->delete('images/' . $image);
            ProductImage::where('filename', $image)->delete();
        }
    }

    public function getUploadedImages($id)
    {
        $images = ProductImage::where('product_id', $id)->get();
        return view('admin.pages.product.images.uploadedImages', ['images' => $images]);
    }

    public function assign($id)
    {
        $product = Product::find($id);
        if (!$product) return redirect()->route('admin.product.assign.index')
            ->with('alert-title', __('admin.product.titles.error'))
            ->with('alert-message', __('admin.product.messages.notFoundError'))
            ->with('alert-class', 'uk-alert-danger');
        $categories = $product->categories()->get();
        $ids = [];
        foreach ($categories as $category) {
            $ids[] = $category->id;
        }
        return view('admin.pages.product.assign.assign', compact('ids', 'product'));
    }

    public function assignSave($id, Request $r)
    {
        $product = Product::find($id);
        if (!$product) return redirect()->route('admin.product.assign.index')
            ->with('alert-title', __('admin.product.titles.error'))
            ->with('alert-message', __('admin.product.messages.notFoundError'))
            ->with('alert-class', 'uk-alert-danger');;

        CategoryProduct::where(['product_id' => $id])->delete();
        foreach ($r->input('categories') as $categoryId) {
            CategoryProduct::create([
                'product_id' => $id,
                'category_id' => $categoryId
            ]);
        }
        return redirect(route('admin.product.assign.index'))
            ->with('alert-title', __('admin.product.titles.saved'))
            ->with('alert-message', __('admin.product.messages.saved'))
            ->with('alert-class', 'uk-alert-success')
            ->with(
                'alert-html',
                '<a href="' . route('admin.product.edit', ['product_id' => $id]) . '">'
                    . __('admin.product.edit') . '</a><br>' .
                    '<a href="' . route('admin.product.images.show', ['product_id' => $id]) . '">'
                    . __('admin.product.messages.editImages') . '</a>'
            );
    }

    public static function showChooser($request, $callbackRoute)
    {
        $products = collect();
        if ($request->input('pickerCategory')) {
            $category = Category::where('id', $request->input('pickerCategory'))->first();
            $products = $category->products()->get();
        }

        if ($request->input('pickerSearch')) {
            $request->flash();
            $products = Product::search($request->input('pickerSearch'));
        }

        $pickerProducts = collect($products)->map(function ($p) use ($callbackRoute) {
            $p->chooseLink = route($callbackRoute, ['product_id' => $p->id]);
            return $p;
        });

        $view = view('admin.components.productPicker', ['products' => $pickerProducts]);
        return $view->render();
    }
}
