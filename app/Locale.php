<?php

namespace App;

use Illuminate\Support\Facades\Auth;

class Locale {
    public static function getLocale() {
        static $locale = null;
        if ($locale) return $locale;

        $session_lang = session('locale');
        if (!$session_lang) {
            $locale = config('app.locale');
            if (Auth::user()) {
                $locale = Auth::user()->locale;
            }
            session(['locale' => $locale]);
            return $locale;
        }
        return $session_lang;
    }
}
