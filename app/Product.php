<?php

namespace App;

use App\I18n\LocalizableModel;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Carbon;

class Product extends LocalizableModel
{
    use HasFactory;

    protected $fillable = ['product_nr', 'visible', 'price', 'weight', 'seo_name'];

    public $localizable = ['name', 'description', 'locale'];

    /**
     * Scope a query to only include active categories
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeVisible($query)
    {
        return $query->where('visible', true);
    }

    public function images()
    {
        return $this->hasMany('App\ProductImage');
    }

    public function link()
    {
        return route('product', ['product' => $this->seo_name]);
    }

    public function categories()
    {
        return $this->belongsToMany('App\Category', 'category_products');
    }

    /**
     * Searches for a Product by string
     */
    public static function search($term)
    {
        $query = Product::query()
            ->select('products.*')
            ->rightJoin('products_locale', function ($join) {
                $join->on('products.id', '=', 'products_locale.product_id');
            })
            ->where('product_nr', 'LIKE', "%{$term}%")
            ->orWhere('name', 'LIKE', "%{$term}%")
            ->orWhere('description', 'LIKE', "%{$term}%")
            ->groupBy('products.id');

        // Add localized fields to select
        foreach (self::getLocalizableFields() as $field) {
            $query->addSelect('products_locale.' . $field);
        }

        return $query->get();
    }

    private static function getLocalizableFields() {
        $instance = new self;
        return $instance->localizable;
    }

    public function generateJsonLD() {
        return [
            "@context" => "https://schema.org/",
            "@type" => "Product",
            "name" => $this->name,
            "image" => [],
            "description" => $this->description,
            "sku" => $this->product_nr,
            "offers" => [
                "@type" => "Offer",
                "url" => $this->link(),
                "priceCurrency" => "USD",
                "price" => $this->price,
                "priceValidUntil" => Carbon::now()->add(1, 'day')->toDateString(),
                "itemCondition" => "https://schema.org/NewCondition",
                "availability" => "https://schema.org/InStock"
            ]
        ];
    }
}
