<?php

namespace App\Classes;

use Stripe\PaymentIntent;
use Stripe\StripeClient;

class Stripe {
    protected static Stripe|null $instance = null;

    public static function instance() {
        if (static::$instance === null) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    public function __construct() {
        $this->loadApiKey();
    }

    public function getCurrentIntent($order, $session) {

        if ($session->has('order.intentId')) {
            $intent = $this->getIntent($session->get('order.intentId'));
            if (!$intent->status === 'requires_payment_method') {
                $intent = $this->createPaymentIntent($order);
            }
        }
        else {
            $intent = $this->createPaymentIntent($order);
        }
        $session->put('order.intentId', $intent->id);
        $this->updateIntentIfNeeded($intent, $order);
        return $intent;
    }

    protected function createPaymentIntent($order) {
        return PaymentIntent::create([
            'amount' => $order->total * 100,
            'currency' => 'chf',
        ]);
    }

    /**
     * Initializes Stripe
     */
    protected function loadApiKey() {
        \Stripe\Stripe::setApiKey(self::getApiKey());
    }

    public static function getApiKey() {
        return env('STRIPE_PRIVATE_KEY');
    }

    public function validateIntent($intent, $order) {
        return ($intent->amount / 100 === $order->total)
            && $intent->status === "succeeded"
            && $intent->amount === $intent->amount_received;
    }

    public function getIntent($intentId) {
        return PaymentIntent::retrieve($intentId);
    }

    protected function updateIntentIfNeeded($intent, $order) {
        $dataToUpdate = [];
        if ($intent->amount / 100 !== $order->total) {
            $dataToUpdate['amount'] = $order->total * 100;
        }

        if (!empty($dataToUpdate)) {
            $stripe = new StripeClient(self::getApiKey());
            $stripe->paymentIntents->update(
                $intent->id,
                $dataToUpdate
              );
        }
    }
}
