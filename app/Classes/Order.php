<?php

namespace App\Classes;

use App\Order as OrderObject;
use App\ShippingOption;
use App\UserAddress;
use Illuminate\Http\Request;
use App\Http\Controllers\CartController;
use Illuminate\Support\Facades\Auth;

class Order {

    public static function getCurrentOrder(Request $r, $cartData = null, $shippingData = null) {
        $userId = Auth::id();
        $cartData = $cartData ?? CartController::getCart($r);
        $shippingData = ShippingOption::find(intval($r->session()->get('order.shippingId')));
        $addressData = UserAddress::find(intval($r->session()->get('order.addressId')));
        $total = self::calculateTotalCost($cartData, $shippingData);
        $userData = collect(['account' =>  Auth::user(), 'address' => $addressData]);
        return new OrderObject([
            'user_id' => $userId,
            'product_data' => serialize($cartData),
            'shipping_data' => serialize($shippingData),
            'user_data' => serialize($userData),
            'total' => $total
        ]);
    }

    public static function calculateTotalCost($cart, $shipping) {
        $total = 0;
        foreach ($cart as $product) {
            $total += $product['count'] * $product['product']->price;
        }
        $total += $shipping->price;

        return $total;
    }

    public static function getCurrentCurrency() {
        return [
            'id' => 'chf',
            'symbol' => 'CHF'
        ];
    }
}