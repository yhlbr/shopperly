<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryProduct extends Model
{
    public $timestamps = false;
    protected $fillable = ['product_id', 'category_id'];
    /**
     * Get the products from a category ID
     */
    public function products()
    {
        return $this->hasMany('App\Product', 'id', 'product_id');
    }

    /**
     * Get the categories from a product ID
     */
    public function categories()
    {
        return $this->hasMany('App\Category', 'id', 'category_id');
    }
}
