<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Page extends Model {
    use HasFactory;

    protected $fillable = [
        'title',
        'description',
        'gjs-html',
        'gjs-components',
        'gjs-assets',
        'gjs-css',
        'gjs-styles',
    ];
}
