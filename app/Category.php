<?php

namespace App;

use App\I18n\LocalizableModel;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Category extends LocalizableModel
{
    use HasFactory;

    protected $localizable = ['name', 'description', 'locale'];

    protected $fillable = ['category_nr', 'visible', 'seo_name'];

    /**
     * Scope a query to only include first level categories.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeRootOnly($query)
    {
        return $query->whereNull('parent_id');
    }

    /**
     * Scope a query to only include active categories
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeVisible($query)
    {
        return $query->where('visible', true);
    }

    public function children()
    {
        return $this->hasMany('App\Category', 'parent_id');
    }

    public function link()
    {
        return route('category', ['category' => $this->seo_name]);
    }

    /**
     * Get the products
     */
    public function products()
    {
        return $this->belongsToMany('App\Product', 'category_products');
    }

    /**
     * Returns all Categories and all children recursively
     * @param string locale
     * @param bool visibleOnly
     * @return Category
     */
    public static function getFullTree($visibleOnly = false)
    {
        $root = Category::rootOnly();
        if ($visibleOnly) $root = $root->visible();
        $root = $root->get();
        foreach ($root as $idx => $cat) {
            $root[$idx] = self::getTreeById($cat->id);
        }
        return $root;
    }

    public static function getTreeById($id, $visibleOnly = false)
    {
        $cat = Category::find($id);
        $children = $cat->children();
        if ($visibleOnly) $children = $children->visible();
        $children = $children->get();
        foreach ($children as $idx => $child) {
            $children[$idx] = self::getTreeById($child->id);
        }
        $cat->children = $children;

        return $cat;
    }
}
