<?php

namespace App\Translations;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductLocale extends Model
{
    use HasFactory;

    protected $table = 'products_locale';
    protected $fillable = ['name', 'description', 'product_id', 'locale'];
    public $timestamps = false;
}
