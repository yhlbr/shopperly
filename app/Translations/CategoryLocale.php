<?php

namespace App\Translations;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CategoryLocale extends Model
{
    use HasFactory;

    protected $table = 'categories_locale';
    protected $fillable = ['name', 'description', 'locale', 'category_id'];
    public $timestamps = false;
}
