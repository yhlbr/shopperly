<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserOauth extends Model
{
    protected $table = 'user_oauth';

    protected $fillable = [
        'user_id',
        'provider',
        'provider_id',
    ];

    protected $hidden = [
        'user_id'
    ];

    public static function addOauthToUser($id, $provider, $provider_id) {
        $oauth = new UserOauth(['user_id' => $id, 'provider' => $provider, 'provider_id' => $provider_id]);
        $oauth->save();
    }

    public static function getUserIdFromOauth($provider, $provider_id) {
        $dataSet = UserOauth::where('provider', $provider)->where('provider_id', $provider_id)->first();
        if (!$dataSet) return false;
        return $dataSet->user_id;
    }
}
