<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = ['user_id', 'intent_id', 'product_data', 'shipping_data', 'user_data', 'total'];
}
