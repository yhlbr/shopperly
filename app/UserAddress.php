<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserAddress extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'address_name',
        'firstname',
        'lastname',
        'company',
        'street',
        'street_no',
        'city',
        'postcode',
        'country'
    ];

    protected $hidden = [
        'user_id'
    ];
}
