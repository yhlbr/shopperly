<?php

namespace App\Providers;

use Exception;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;


class SettingsServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap Shop Settings
     *
     * @return void
     * @throws Exception
     */
    public function boot()
    {
        if (!Schema::hasTable('settings')) return;
        $settings = cache()->remember('settings', 60, function () {
            return \App\Setting::pluck('value', 'key')->all();
        });

        $settings['Languages'] = explode(',', $settings['Languages']);
        config()->set('settings', $settings);
        $this->registerDefaultSettings();
    }

    public function registerDefaultSettings() {
        config()->set('mail.from.name', config('settings.ShopName'));
        config()->set('mail.from.address', config('settings.Email', 'noreply@example.com'));
        $this->app->booted(function() {
            config()->set('services.google.redirect', route('login_oauth_callback', ['provider' => 'google']));
        });
    }
}
