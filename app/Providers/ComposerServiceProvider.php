<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;
use App\Category;
use App\Locale;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     * @throws \Exception
     */
    public function boot()
    {
        $tree = cache()->rememberForever('categoryTree.' . Locale::getLocale(), function () {
            return Category::getFullTree();
        });

        View::composer(
            [
                'components.nav',
                'admin.components.productPicker',
                'admin.pages.product.assign.assign',
                'admin.pages.category.edit.index',
            ]
            , function ($view) use ($tree) {
            $view->with('categories', $tree);
        });
    }
}
