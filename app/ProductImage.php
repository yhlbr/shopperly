<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;


class ProductImage extends Model
{
    protected $fillable = ['id', 'filename', 'product_id', 'title', 'seo_filename'];
    public $timestamps = false;

    public function getLinkAttribute() {
        return route('product.image', ['filename' => $this->seo_name]);
    }

    public function getSeoNameAttribute() {
        $seo_slug = '';
        if ($this->title) {
            $seo_slug = '-' . Str::slug($this->title);
        }

        if ($this->seo_filename) {
            $seo_slug = '-' . $this->seo_filename;
        }

        return $this->id . $seo_slug .  '.' . $this->file_ending;
    }

    public function getFileEndingAttribute() {
        preg_match_all('/.*\.(.*)/m', $this->filename, $matches, PREG_SET_ORDER);
        return $matches[0][1];
    }

    public function getFilepathAttribute() {
        return 'images/' . $this->filename;
    }
}
