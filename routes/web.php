<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index')->name('index');
Route::get('/caddy-check', 'CaddyController@check');

Auth::routes(['verify' => true]);

Route::get('/logout', 'Auth\LoginController@logout')->name('get_logout');

// OAuth-Routes
Route::get('/login/{provider}', 'OauthController@redirect')->name('login_oauth');
Route::get('/login/{provider}/callback', 'OauthController@callback')->name('login_oauth_callback');


// Main Webshop Functions
Route::get('/category/{category}', 'CategoryController@index')->name('category');
Route::get('/product/{product}', 'ProductController@index')->name('product');
Route::get('/page/{page}', 'PageController@index')->name('page');
Route::get('/page/{page}/styles.css', 'PageController@styles')->name('page.styles');

Route::get('/product/image/{filename?}', 'FileController@serveProductImage')->name('product.image')
    ->where('filename', '(.*)');

Route::get('/cart/', 'CartController@cart')->name('cart');
Route::get('/cart/{product_id}/add', 'CartController@add')
    ->name('cart.add')
    ->where(['product_id' => '[0-9]+']);

Route::get('/cart/modify/{index}/{amount}', 'CartController@modify')
    ->name('cart.modify')
    ->where(['index' => '[0-9]+', 'name' => '[0-9]+']);

Route::prefix('/order')->name('order.')->middleware('auth')->group(function() {
    Route::get('/checkout', 'OrderController@checkout')->name('checkout');
    Route::post('/checkout', 'OrderController@saveCheckout')->name('saveCheckout');
    Route::get('/switchAddress/{address_id}', 'OrderController@switchAddress')
        ->name('switchAddress')
        ->where(['address_id' => '[0-9]+']);
        Route::get('/shipping', 'OrderController@shipping')->name('shipping');
        Route::get('/shipping/{option_id}', 'OrderController@saveShipping')->name('saveShipping');
        Route::get('/confirmation', 'OrderController@confirmation')->name('confirmation');
        Route::post('/complete', 'OrderController@complete')->name('complete');
        Route::get('/success', 'OrderController@success')->name('success');
});

// Account functions
Route::prefix('/account')->name('account.')->middleware('auth')->group(function() {
    Route::get('/', 'AccountController@index')->name('index');
    Route::get('/edit/', 'AccountController@edit')->name('edit');
    Route::get('/address/{address_id}/edit/', 'AccountController@editAddress')->name('address.edit');
    Route::get('/address/add/', 'AccountController@addAddress')->name('address.add');
    Route::post('/address/add/', 'AccountController@addAddress')->name('address.saveAdd');
    Route::get('/password/', 'AccountController@password')->name('password');
    Route::post('/password/', 'AccountController@updatePassword')->name('password.update');
});

// Admin Routes
Route::namespace('Admin')
    ->middleware(['auth', 'admin'])
    ->prefix('/admin')
    ->name('admin.')
    ->group(function () {
        Route::get('/', 'IndexController@Index')->name('index');
        Route::prefix('product')->name('product.')->group(function () {
            Route::get('/add/', 'ProductController@add')->name('add');
            Route::post('/add/', 'ProductController@addSave')->name('add.save');

            Route::get('/edit/', 'ProductController@editIndex')->name('edit.index');
            Route::get('/edit/{product_id}', 'ProductController@edit')->name('edit');
            Route::post('/edit/{product_id}', 'ProductController@editSave')->name('edit.save');


            Route::prefix('images')->name('images.')->group(function () {
                Route::get('/', 'ProductController@imagesIndex')->name('index');
                Route::get('/{product_id}', 'ProductController@images')->name('show');
                Route::post('/{product_id}/upload', 'ProductController@imagesUpload')->name('upload');
                Route::get('/{product_id}/current', 'ProductController@getUploadedImages')->name('current');
                Route::get('/delete/{image_name}', 'ProductController@deleteImage')->name('delete');
            });
            Route::get('/assign/', 'ProductController@assignIndex')->name('assign.index');
            Route::get('/assign/{product_id}', 'ProductController@assign')->name('assign.show');
            Route::post('/assign/{product_id}', 'ProductController@assignSave')->name('assign.save');
        });
        Route::prefix('category')->name('category.')->group(function () {
            Route::get('/add/', 'CategoryController@add')->name('add');
            Route::post('/add/', 'CategoryController@addSave')->name('add.save');

            Route::get('/edit/', 'CategoryController@editIndex')->name('edit.index');
            Route::get('/edit/{category_id}', 'CategoryController@edit')->name('edit');
            Route::post('/edit/{category_id}', 'CategoryController@editSave')->name('edit.save');
        });
        Route::prefix('page')->name('page.')->group(function () {
            Route::get('/add/', 'PageController@add')->name('add');
            Route::post('/add/', 'PageController@addSave')->name('add.save');

            Route::get('/edit/', 'PageController@editIndex')->name('edit.index');
            Route::get('/edit/{page_id}', 'PageController@edit')->name('edit');
            Route::post('/edit/{page_id}', 'PageController@editSave')->name('edit.save');

            Route::post('/edit/content/{page_id}', 'PageController@saveEditor')->name('content.save');
            Route::get('/edit/content/{page_id}', 'PageController@loadEditor')->name('content.load');

            Route::get('/assets/', 'PageController@assets')->name('assets');
            Route::post('/assets/upload', 'PageController@uploadAssets')->name('assets.upload');
        });
    });
