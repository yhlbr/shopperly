<?php

return [
    'productPicker' => [
        'byCategory' => 'By Category'
    ],
    'product'       => [
        'edit'          => 'Edit product',
        'localizations' => 'Localizations',
        'details'       => 'Details',
        'name'          => 'Name',
        'description'   => 'Description',
        'price'         => 'Price',
        'weight'        => 'Weight',
        'productNr'     => 'Product Nr.',
        'visible'       => 'Visible',
        'assign'        => 'Assign Categories',
        'titles'        => [
            'saved' => 'Product saved',
            'error' => 'Error',
        ],
        'messages'      => [
            'saved'                   => 'Product was updated successfully.',
            'notFoundError'           => 'Product not found.',
            'saveError'               => 'Error while saving Product.',
            'deleteImageConfirmation' => 'Delete Image?',
            'editImages'              => 'Edit Images',
            'assignCategories'        => 'Assign Categories'
        ]
    ],
    'category'      => [
        'localizations' => 'Localizations',
        'name'          => 'Name',
        'description'   => 'Description',
        'details'       => 'Details',
        'categoryNr'    => 'Category Nr.',
        'visible'       => 'Visible',
        'titles'        => [
            'saved' => 'Category saved',
            'error' => 'Error',
        ],
        'messages'      => [
            'saved'         => 'Category was updated successfully.',
            'notFoundError' => 'Category not found.',
            'saveError'     => 'Error while saving Category.',
        ]
    ],
    'page' => [
        'messages' => [
            'saved' => 'Page saved successfully.'
        ],
    ],
];
