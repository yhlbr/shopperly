<?php

/* Common localized strings, used all over the shop */
return [
    'product'        => 'product|products',
    'category'       => 'category|categories',
    'client'         => 'client|clients',
    'order'          => 'order|orders',
    'page'           => 'page|pages',
    'save'           => 'Save',
    'edit'           => 'Edit',
    'cancel'         => 'Cancel',
    'add'            => 'Add',
    'delete'         => 'Delete',
    'image'          => 'image|images',
    'search'         => 'Search',
    'price'          => 'Price',
    'total'          => 'Total',
    'details'        => 'Details',
    'email'          => 'E-Mail',
    'password'       => 'Password',
    'administration' => 'Administration',
    'choose'         => 'Choose',
];
