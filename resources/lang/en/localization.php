<?php

return [
    'languages' => [
        'en' => 'English',
        'de' => 'German',
        'fr' => 'French',
    ],
    'language'  => 'Language',
    'defaultLanguage' => 'Default language'
];
