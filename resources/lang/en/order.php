<?php

return [
    'addToCart'          => 'Add to cart',
    'cart'               => 'Cart',
    'checkout'           => 'Checkout',
    'switchAddress'      => 'Switch Address',
    'addressName'        => 'Address Name',
    'address'            => 'Address',
    'defaultAddressName' => 'Home',
    'firstname'          => 'Firstname',
    'lastname'           => 'Lastname',
    'company'            => 'Company',
    'street'             => 'Street',
    'houseNo'            => 'House Number',
    'postcode'           => 'Postcode',
    'city'               => 'City',
    'country'            => 'Country',
    'count'              => 'Count',
    'shipping'           => 'Shipping',
    'confirmation'       => 'Confirmation',
    'buy'                => 'Buy',
    'complete'           => 'Complete Order',
    'success'            => 'Thank You for your Order',
    'successMessage'     => 'A Confirmation of your Order will be sent to you shortly.',
    'addAddress'         => 'Add Address',
    'payment'            => [
        'validationError' => 'Verification of payment failed. Please try again.'
    ]
];
