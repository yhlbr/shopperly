import jQuery from "jquery";
import _ from 'lodash';
import UIkit from "uikit";
import * as Icons from "uikit/dist/js/uikit-icons";
import popperjs from "popper.js";
import axios from 'axios';

window._ = _;
window.axios = axios;

window.UIkit = UIkit;
window.Icons = Icons;

UIkit.use(window.Icons);

try {
    window.Popper = popperjs;

    window.$ = window.jQuery = jQuery;
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
} catch (e) {}

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
