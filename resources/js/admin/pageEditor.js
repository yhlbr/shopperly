window.PageEditor = class PageEditor {
    constructor(el, id, styles) {
        styles = styles || [];
        this.editor = grapesjs.init({
                                        container     : el,
                                        plugins       : ['gjs-preset-webpage'],
                                        pluginsOpts   : {
                                            'gjs-preset-webpage': {
                                                // options
                                            }
                                        },
                                        canvas        : {
                                            styles: styles
                                        },
                                        storageManager: {
                                            type           : 'remote',
                                            stepsBeforeSave: 3,
                                            urlStore       : route('admin.page.content.load', id),
                                            urlLoad        : route('admin.page.content.load', id),
                                            headers        : {
                                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                            },
                                        },
                                        assetManager  : {
                                            assets : [],
                                            headers: {
                                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                            },
                                            upload : route('admin.page.assets.upload')
                                        }
                                    });
        this.loadAssets();
    }

    save(callback) {
        this.editor.store(res => callback(res));
    }

    loadAssets() {
        axios.get(route('admin.page.assets')).then((response) => {
            if (response.status !== 200) return;
            this.editor.AssetManager.add(response.data);
        });
    }
}
