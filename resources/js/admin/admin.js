$('#admin-menu-toggle').click((e) => {
    e.preventDefault();
    $('#sidenav-container').toggle('hidden');
    if ($('#page-container').get(0).style.width != '100%') {
        $('#page-container').animate({width: '100%'});
    }
    else {
        $('#page-container').css('width', '');
    }
});

window.notification = (text, status) => {
    UIkit.notification({message: text, status: status});
}
