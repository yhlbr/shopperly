@if(Auth::user()->addresses()->count() > 0)
    <button class="uk-button uk-button-link {{ $customButtonClasses ?? '' }}" type="button">{{ __('order.switchAddress') }}</button>
    <div uk-dropdown="mode: click">
        <ul class="uk-nav uk-dropdown-nav">
            @foreach(Auth::user()->addresses()->get() as $a)
                <li><a href="{{ route('order.switchAddress', ['address_id' => $a->id]) }}">{{ $a->address_name }}</a></li>
            @endforeach
            @if(isset($addAddressRoute))
                <li><a href="{{ $addAddressRoute }}">
                        <span uk-icon="icon: plus; ratio: 0.7"></span>
                        {{ __('common.add') }}
                    </a></li>
            @endif
        </ul>
    </div>
@endif
