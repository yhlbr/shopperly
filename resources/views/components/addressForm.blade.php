<div class="uk-margin" uk-grid>
    <div class="uk-width-1-2">
        <label class="uk-form-label" for="firstname">{{ __('order.firstname') }}</label>
        <div class="uk-form-controls">
            <input class="uk-input" id="firstname" name="firstname" type="text" value="{{ old('firstname') ?? $address->firstname ?? '' }}">
        </div>
    </div>

    <div class="uk-width-1-2">
        <label class="uk-form-label" for="lastname">{{ __('order.lastname') }}</label>
        <div class="uk-form-controls">
            <input class="uk-input" id="lastname" name="lastname" type="text" value="{{ old('lastname') ?? $address->lastname ?? '' }}">
        </div>
    </div>
</div>

<div class="uk-margin">
    <label class="uk-form-label" for="company">{{ __('order.company') }}</label>
    <div class="uk-form-controls">
        <input class="uk-input" id="company" name="company" type="text" value="{{ old('company') ?? $address->company ?? '' }}">
    </div>
</div>

<div class="uk-margin" uk-grid>
    <div class="uk-width-3-4">
        <label class="uk-form-label" for="street">{{ __('order.street') }}</label>
        <div class="uk-form-controls">
            <input class="uk-input" id="street" name="street" type="text" value="{{ old('street') ?? $address->street ?? '' }}">
        </div>
    </div>

    <div class="uk-width-1-4">
        <label class="uk-form-label" for="street_no">{{ __('order.houseNo') }}</label>
        <div class="uk-form-controls">
            <input class="uk-input" id="street_no" name="street_no" type="text" value="{{ old('street_no') ?? $address->street_no ?? '' }}">
        </div>
    </div>
</div>
<div class="uk-margin" uk-grid>
    <div class="uk-width-1-4">
        <label class="uk-form-label" for="postcode">{{ __('order.postcode') }}</label>
        <div class="uk-form-controls">
            <input class="uk-input" id="postcode" name="postcode" type="text" value="{{ old('postcode') ?? $address->postcode ?? '' }}">
        </div>
    </div>

    <div class="uk-width-3-4">
        <label class="uk-form-label" for="city">{{ __('order.city') }}</label>
        <div class="uk-form-controls">
            <input class="uk-input" id="city" name="city" type="text" value="{{ old('city') ?? $address->city ?? '' }}">
        </div>
    </div>
</div>

<div class="uk-margin">
    <label class="uk-form-label" for="country">{{ __('order.country') }}</label>
    <div class="uk-form-controls">
        <input class="uk-input uk-width-1-2" id="country" name="country" type="text" value="{{ old('country') ?? $address->country ?? '' }}">
    </div>
</div>
