<nav class="uk-navbar-container" uk-navbar>
    <div class="uk-navbar-left">
        <button type="button" class="uk-navbar-toggle uk-hidden@m" uk-navbar-toggle-icon uk-toggle="target: #mobile-nav"></button>
        <ul class="uk-navbar-nav uk-visible@m">
            <a href="{{ route('index') }}" class="uk-navbar-item uk-logo">{{ config('settings.ShopName') }}</a>

            @foreach($categories as $cat)
                @if(count($cat->children) > 0)
                    <li>
                        <a href="{{ $cat->link() }}">{{ $cat->name }}</a>
                        <div class="uk-navbar-dropdown">
                            <ul class="uk-nav uk-navbar-dropdown-nav">
                                @include('components.nav-children', ['children' => $cat->children])
                            </ul>
                        </div>
                    </li>
                @else
                    <li><a href="{{ $cat->link() }}">{{ $cat->name }}</a></li>
                @endif
            @endforeach
        </ul>
        <ul class="uk-navbar-nav uk-hidden@m">
            <a href="{{ route('index') }}" class="uk-navbar-item uk-logo">{{ config('settings.ShopName') }}</a>
        </ul>
    </div>
    <div class="uk-navbar-right">
        @if(Auth::id())
        <a href="{{ route('account.index') }}" class="uk-navbar-item uk-margin-right">
            <span uk-icon="icon: user; ratio: 1.5;"></span>
        </a>
        <div uk-dropdown>
            <ul class="uk-nav uk-dropdown-nav">
                <li><a href="{{ route('account.index') }}">{{ __('user.account') }}</a></li>
            </ul>
            <ul class="uk-nav uk-dropdown-nav">
                <li><a href="{{ route('get_logout') }}">{{ __('user.logout') }}</a></li>
            </ul>
        </div>
        @else
        <a href="{{ route('login') }}" class="uk-navbar-item uk-margin-right">
            <span uk-icon="icon: user; ratio: 1.5;"></span>
        </a>
        @endif
        <a href="{{ route('cart') }}" class="uk-navbar-item uk-margin-right">
            <span uk-icon="icon: cart; ratio: 1.5;"></span>
        </a>
    </div>
</nav>
<div id="mobile-nav" uk-offcanvas>
    <div class="uk-offcanvas-bar">
        <ul class="uk-nav uk-nav-default">
            @foreach($categories as $cat)
                @if(count($cat->children) > 0)
                    <li>
                        <a href="{{ $cat->link() }}">{{ $cat->name }}</a>
                        <div class="uk-navbar-dropdown">
                            <ul class="uk-nav uk-navbar-dropdown-nav">
                                @include('components.nav-children', ['children' => $cat->children])
                            </ul>
                        </div>
                    </li>
                @else
                    <li><a href="{{ $cat->link() }}">{{ $cat->name }}</a></li>
                @endif
            @endforeach
        </ul>
    </div>
</div>
