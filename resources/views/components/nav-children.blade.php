@foreach($children as $child)
<li class="{{ count($child->children) > 0 ? 'uk-parent' : ''}}">
    <a href="{{ $child->link() }}">{{ $child->name }}</a>
    @if(count($child->children) > 0)
        <ul class="uk-nav-sub">
            @include('components.nav-children', ['children' => $child->children])
        </ul>
    @endif
</li>
@endforeach