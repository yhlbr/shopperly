<div class="uk-card uk-card-default uk-card-body">
    <h3 class="uk-card-title">
      {{ __('order.address') }}
        @include('components.addressSwitcher', ['customButtonClasses' => 'uk-align-right'])
    </h3>
    <p>{{ $address->firstname }} {{ $address->lastname }}<br>
    @if($address->company)
        {{ $address->company }}<br>
    @endif
    {{ $address->street }} {{ $address->street_no }}<br>
    {{ $address->postcode }} {{ $address->city }}<br>
    {{ $address->country }}</p>
    <a class="uk-button uk-button-primary uk-align-right" href="{{ route('account.address.edit', ['address_id' => $address->id]) }}">{{ __('common.edit') }}</a>
</div>
