@if(session()->has('success'))
    <div class="uk-alert-success" uk-alert>
        <a class="uk-alert-close" uk-close></a>
        {{ session()->get('success') }}
    </div>
@endif
@if(session()->has('error'))
    <div class="uk-alert-danger" uk-alert>
        <a class="uk-alert-close" uk-close></a>
        {{ session()->get('error') }}
    </div>
@endif
@if(session()->has('info'))
    <div class="uk-alert-primary" uk-alert>
        <a class="uk-alert-close" uk-close></a>
        {{ session()->get('info') }}
    </div>
@endif
