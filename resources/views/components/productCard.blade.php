<div>
    <div class="uk-card uk-card-default uk-card-hover">
        <div class="uk-card-media-top">
            <img src="{{ $product->images->first()->link ?? 'https://via.placeholder.com/600x400' }}" alt="Placeholder">
        </div>
        <div class="uk-card-body">
            <h3 class="uk-card-title">
                <a class="uk-link-heading" href="{{ $product->link() }}">{{ $product->name }}</a>
            </h3>
            <p>{{ $product->description }}</p>
        </div>
        <div class="uk-card-footer">
            <a href="{{ $product->link() }}" class="uk-button uk-button-text">{{ __('common.details') }}</a>
            <p class="uk-align-right">{{ Str::currency($product->price) }}</p>
        </div>
    </div>
</div>
