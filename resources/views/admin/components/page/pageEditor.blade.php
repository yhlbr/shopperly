@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/grapesjs/0.16.34/grapes.min.js" integrity="sha512-BJ3wA81pBNTM/DvXvu1z0DF06thmTMJhpWIq4g0mHco5Uet6tE8wCLAvFDrOhLPTvnpn+quAVMwzBQoTuahYvw==" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/grapesjs-preset-webpage@0.1.11/dist/grapesjs-preset-webpage.min.js"></script>
    <script src="{{ mix('js/admin/pageEditor.js') }}"></script>
    <script>
        $(document).ready(function () {
            var styles = ['{{ mix('css/app.css') }}'];
            var editor = new PageEditor(
                $('#editor').get(0),
                {{ $id }},
                styles
            );

            $('#btn-save').click(function () {
                editor.save(function () {
                    notification('{{ __('admin.page.messages.saved') }}', 'success');
                });
            });
        });
    </script>
@endpush
@push('styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/grapesjs/0.16.34/css/grapes.min.css" integrity="sha512-uVYer3Rc1iMaFW6eT4N3FCtnyHebJqmX45NqjuR+FTj7OgKkNaTZhh/jZZD6YeOiXUoNOkuggpr9AKvIItNYrg=="
          crossorigin="anonymous"/>
@endpush
<div class="uk-clearfix">
    <button id="btn-save" class="uk-button uk-button-primary uk-margin-bottom uk-align-right" type="button">{{ __('common.save') }}</button>
</div>
<div class="uk-clearfix" id="editor"></div>
