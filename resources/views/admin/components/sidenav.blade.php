<ul class="uk-nav-default uk-nav-parent-icon" uk-nav>
    <li class="uk-parent">
        <a href="#">{{ ucfirst(trans_choice('common.product', 2)) }}</a>
        <ul class="uk-nav-sub">
            <li><a href="{{ route('admin.product.add') }}">{{ __('common.add') }}</a></li>
            <li><a href="{{ route('admin.product.edit.index') }}">{{ __('common.edit') }}</a></li>
            <li><a href="#">{{ __('common.delete') }}</a></li>
            <li><a href="{{ route('admin.product.images.index') }}">{{ ucfirst(trans_choice('common.image', 2)) }}</a></li>
            <li><a href="{{ route('admin.product.assign.index') }}">{{ __('admin.product.assign') }}</a></li>
        </ul>
    </li>
    <li class="uk-parent">
        <a href="#">{{ ucfirst(trans_choice('common.category', 2)) }}</a>
        <ul class="uk-nav-sub">
            <li><a href="{{ route('admin.category.add') }}">{{ __('common.add') }}</a></li>
            <li><a href="{{ route('admin.category.edit.index') }}">{{ __('common.edit') }}</a></li>
            <li><a href="#">{{ __('common.delete') }}</a></li>
        </ul>
    </li>
    <li class="uk-parent">
        <a href="#">{{ ucfirst(trans_choice('common.page', 2)) }}</a>
        <ul class="uk-nav-sub">
            <li><a href="{{ route('admin.page.add') }}">{{ __('common.add') }}</a></li>
            <li><a href="{{ route('admin.page.edit.index') }}">{{ __('common.edit') }}</a></li>
            <li><a href="#">{{ __('common.delete') }}</a></li>
        </ul>
    </li>
</ul>
