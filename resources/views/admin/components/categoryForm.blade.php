<form method="POST" action="{{ $formAction }}" class="uk-form-stacked">
    <fieldset class="uk-fieldset">
        @csrf

        <h1 class="uk-heading-divider">
            {{ $category->name ?? ucfirst(trans_choice(__('common.category'), 1)) }}
        </h1>

        <h2 class="uk-heading-divider">
            {{ __('admin.category.localizations') }}
        </h2>

        <ul uk-tab id="tab-headings">
            @foreach (config('settings.Languages') as $locale)
            <li><a href="#">{{ __('localization.languages.' . $locale) }}</a></li>
            @endforeach
        </ul>
        <ul class="uk-switcher uk-margin">
            @foreach (config('settings.Languages') as $locale)
            <li>
                <div class="uk-margin">
                    <label class="uk-form-label" for="name-{{ $locale }}">
                        {{ __('admin.category.name') }}
                    </label>
                    <div class="uk-form-controls">
                        <input
                            class="uk-input"
                            id="name-{{ $locale }}"
                            name="locales[{{ $locale }}][name]"
                            type="text"
                            value="@if(isset($category)){{ $category->translations->where('locale', $locale)->first()->name ?? '' }}@endif">
                    </div>
                </div>
                <div class="uk-margin">
                    <label class="uk-form-label" for="description-{{ $locale }}">
                        {{ __('admin.category.description') }}
                    </label>
                    <div class="uk-form-controls">
                        <textarea id="description-{{ $locale }}" name="locales[{{ $locale }}][description]" class="uk-textarea" rows="5">@if(isset($category)){{ $category->translations->where('locale', $locale)->first()->description ?? '' }}@endif</textarea>
                    </div>
                </div>

            </li>
            @endforeach
        </ul>

        <h2 class="uk-heading-divider">{{ __('admin.category.details') }}</h2>

        <div class="uk-margin">
            <label class="uk-form-label" for="category_nr">{{ __('admin.category.categoryNr') }}</label>
            <div class="uk-form-controls">
                <input class="uk-input" name="category_nr" type="text" value="@if(isset($category)){{ $category->category_nr ?? '' }}@endif">
            </div>
        </div>

        <div class="uk-margin">
            <div class="uk-form-controls uk-form-controls-text">
                <label><input class="uk-checkbox" type="checkbox" name="visible" @if(isset($category)){{ $category->visible ? 'checked' : '' }} @else checked @endif> {{ __('admin.category.visible') }}</label>
            </div>
        </div>

        <div class="uk-margin">
            <input class="uk-button uk-button-primary" type="submit" value="{{ __('common.save') }}">
        </div>
    </fieldset>
</form>
