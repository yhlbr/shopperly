<div class="uk-grid-divider" uk-grid>
    <div class="uk-width-1-3@l uk-width-1-1@s">
        <h1 class="uk-heading-divider">{{ __('common.search') }}</h1>
        <form class="uk-search uk-search-default" method="get" action="{{ URL::current() }}">
            <a href="" uk-search-icon></a>
            <input name="pickerSearch" class="uk-search-input" type="search" placeholder="" value="{{ old('pickerSearch') }}">
        </form>
        <h1 class="uk-heading-divider">{{ __('admin.productPicker.byCategory') }}</h1>
        <ul class="uk-nav uk-nav-default">
            @each('admin.components.categoriesTree', $categories, 'category')
        </ul>
    </div>
    <div class="uk-width-2-3@l uk-width-1-1@s">
        <h1 class="uk-heading-divider">{{ ucfirst(trans_choice('common.product', 2)) }}</h1>
        @if(!empty($products))
        <ul class="uk-list uk-list-divider">
            @foreach($products as $p)
            <li><a href="{{ $p->chooseLink }}">[{{ $p->product_nr }}] {{ $p->name }}</a></li>
            @endforeach
        </ul>
        @endisset
    </div>
</div>
