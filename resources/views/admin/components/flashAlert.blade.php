<div class="{{ Session::get('alert-class', '') }}" uk-alert>
    <a class="uk-alert-close" uk-close></a>
    @if(Session::has('alert-title'))
    <h3>{{ Session::get('alert-title') }}</h3>
    @endif
    @if(Session::has('alert-message'))
    <p>{{ Session::get('alert-message') }}</p>
    @endif
    @if(Session::has('alert-html'))
    {!! Session::get('alert-html') !!}
    @endif
</div>