<form method="POST" action="{{ $formAction }}" class="uk-form-stacked">
    <fieldset class="uk-fieldset">
        @csrf

        <h1 class="uk-heading-divider">
            {{ $product->name ?? ucfirst(trans_choice(__('common.product'), 1)) }}
        </h1>

        <h2 class="uk-heading-divider">
            {{ __('admin.product.localizations') }}
        </h2>

        <ul uk-tab id="tab-headings">
            @foreach (config('settings.Languages') as $locale)
            <li><a href="#">{{ __('localization.languages.' . $locale) }}</a></li>
            @endforeach
        </ul>
        <ul class="uk-switcher uk-margin">
            @foreach (config('settings.Languages') as $locale)
            <li>
                <div class="uk-margin">
                    <label class="uk-form-label" for="name-{{ $locale }}">
                        {{ __('admin.product.name') }}
                    </label>
                    <div class="uk-form-controls">
                        <input
                            class="uk-input"
                            id="name-{{ $locale }}"
                            name="locales[{{ $locale }}][name]"
                            type="text"
                            value="@if(isset($product)){{ $product->translations->where('locale', $locale)->first()->name ?? '' }}@endif">
                    </div>
                </div>
                <div class="uk-margin">
                    <label class="uk-form-label" for="description-{{ $locale }}">
                        {{ __('admin.product.description') }}
                    </label>
                    <div class="uk-form-controls">
                        <textarea id="description-{{ $locale }}" name="locales[{{ $locale }}][description]" class="uk-textarea" rows="5">@if(isset($product)){{ $product->translations->where('locale', $locale)->first()->description ?? '' }}@endif</textarea>
                    </div>
                </div>

            </li>
            @endforeach
        </ul>

        <h2 class="uk-heading-divider">{{ __('admin.product.details') }}</h2>

        <div class="uk-margin">
            <label class="uk-form-label" for="product_nr">{{ __('admin.product.productNr') }}</label>
            <div class="uk-form-controls">
                <input class="uk-input" name="product_nr" type="text" value="@if(isset($product)){{ $product->product_nr ?? '' }}@endif">
            </div>
        </div>

        <div class="uk-margin">
            <div uk-grid>
                <div class="uk-width-1-3">
                    <label class="uk-form-label" for="price">{{ __('admin.product.price') }}</label>
                    <div class="uk-form-controls">
                        <input class="uk-input" name="price" type="number" step="0.01" value="@if(isset($product)){{ $product->price ?? '' }}@endif">
                    </div>
                </div>
                <div class="uk-width-1-3">
                    <label class="uk-form-label" for="weight">{{ __('admin.product.weight') }}</label>
                    <div class="uk-form-controls">
                        <input class="uk-input" name="weight" type="number" value="@if(isset($product)){{ $product->weight ?? '' }}@endif">
                    </div>
                </div>
            </div>
        </div>

        <div class="uk-margin">
            <div class="uk-form-controls uk-form-controls-text">
                <label><input class="uk-checkbox" type="checkbox" name="visible" @if(isset($product)){{ $product->visible ? 'checked' : '' }} @else checked @endif> {{ __('admin.product.visible') }}</label>
            </div>
        </div>

        <div class="uk-margin">
            <input class="uk-button uk-button-primary" type="submit" value="{{ __('common.save') }}">
        </div>
    </fieldset>
</form>
