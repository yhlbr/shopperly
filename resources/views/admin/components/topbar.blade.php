<nav class="uk-navbar-container uk-margin" uk-navbar>
    <div class="uk-navbar-left">
        <a href="#" class="uk-navbar-item" id="admin-menu-toggle">
            <span class="uk-icon uk-margin-small-right" uk-icon="icon: menu"></span>
        </a>
        <a class="uk-navbar-item uk-logo" href="{{ route('admin.index') }}">
            {{ config('settings.ShopName') }}
        </a>
    </div>
    <div class="uk-navbar-right">
        <a href="{{ route('index') }}" class="uk-navbar-item" target="_blank">
            <span class="uk-icon" uk-icon="icon: push"></span>
        </a>
    </div>
</nav>
