@if(count($category->children) > 0)
<li class="uk-parent {{ Request::query('pickerCategory') == $category->id ? 'uk-active' : '' }}">
    <a href="{{ URL::current() . '?pickerCategory=' . $category->id }}">{{ $category->name }}</a>
    <ul class="uk-nav-sub">
        @each('admin.components.categoriesTree', $category->children, 'category')
    </ul>
</li>
@else
<li class="{{ Request::query('pickerCategory') == $category->id ? 'uk-active' : '' }}">
    <a href="{{ URL::current() . '?pickerCategory=' . $category->id }}">
        {{ $category->name }}
    </a>
</li>
@endif