@extends('layouts.admin')

@section('content')
@if ($errors->any())
<div class="uk-alert-danger" uk-alert>
    <a class="uk-alert-close" uk-close></a>
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
@include('admin.components.productForm')
@endsection