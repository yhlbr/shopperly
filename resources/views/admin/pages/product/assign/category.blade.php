<div class="uk-margin-small uk-margin-left">
    <label><input
        class="uk-checkbox"
        name="categories[]"
        type="checkbox"
        value="{{ $category->id }}"
        {{ in_array($category->id, $ids) ? 'checked' : '' }}
    >
        {{ $category->name }}
    </label>
    @foreach($category->children as $category)
        @include('admin.pages.product.assign.category', compact('category', 'ids'))
    @endforeach
</div>