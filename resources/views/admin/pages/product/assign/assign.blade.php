@extends('layouts.admin')

@section('content')
@if ($errors->any())
<div class="uk-alert-danger" uk-alert>
    <a class="uk-alert-close" uk-close></a>
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<form method="POST" action="{{ route('admin.product.assign.save', ['product_id' => $product->id]) }}" class="uk-form-stacked">
    @csrf

    @foreach($categories as $category)
        @include('admin.pages.product.assign.category', compact('category', 'ids'))
    @endforeach

    <div class="uk-margin">
        <input class="uk-button uk-button-primary" type="submit" value="{{ __('common.save') }}">
    </div>
</form>
@endsection