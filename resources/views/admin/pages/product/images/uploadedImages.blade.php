<div uk-grid>
    @foreach($images as $image)
    <div class="uk-width-1-4@l uk-width-1-1@s">
        <div uk-lightbox>
            <a href="{{ $image->link }}">
                <img src="{{ $image->link }}" alt="">
            </a>
        </div>
    </div>
    <div class="uk-width-1-4@l uk-width-2-1@s">
        <a data-href="{{ route('admin.product.images.delete', ['image_name' => $image->filename]) }}" class="delete-button uk-button uk-button-danger">{{ __('common.delete') }}</a>
    </div>
    @endforeach
</div>
<script>
    $('.delete-button').click(function() {
        if (confirm('{{ __("admin.product.messages.deleteImageConfirmation") }}')) {
            $(this).prepend('<div uk-spinner="ratio: 0.8" class="uk-margin-right"></div>');
            $.get($(this).data('href'), function() {
                reloadImages();
            });
        }
    });
</script>
