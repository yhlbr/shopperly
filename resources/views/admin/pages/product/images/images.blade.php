@extends('layouts.admin')

@section('content')

<form method="POST" action="" class="uk-form-stacked uk-margin-bottom">
    <fieldset class="uk-fieldset">
        @csrf
        <div class="js-upload" uk-form-custom>
            <input type="file" name="files" multiple>
            <button class="uk-button uk-button-default" type="button" tabindex="-1">Select</button>
        </div>
        <progress id="progressbar" class="uk-progress" value="" max="" hidden></progress>
    </fieldset>
</form>
<div id="uploadedImages"></div>
@endsection
@push('scripts')
<script>
    var bar = document.getElementById('progressbar');

    UIkit.upload('.js-upload', {
        url: '{{ route("admin.product.images.upload", ["product_id" => $product->id]) }}',
        multiple: true,

        beforeSend: function(e) {
            e.headers = {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            };
        },
        loadStart: function(e) {
            bar.removeAttribute('hidden');
            bar.max = e.total;
            bar.value = e.loaded;
        },

        progress: function(e) {
            bar.max = e.total;
            bar.value = e.loaded;
        },

        loadEnd: function(e) {
            bar.max = e.total;
            bar.value = e.loaded;
        },

        completeAll: function() {
            setTimeout(function() {
                bar.setAttribute('hidden', 'hidden');
            }, 1000);
            reloadImages();
        }
    });

    reloadImages();

    function reloadImages() {
        $('#uploadedImages').load('{{ route("admin.product.images.current", ["product_id" => $product->id]) }}');
    }
</script>
@endpush