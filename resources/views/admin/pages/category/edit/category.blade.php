<div class="uk-margin-small uk-margin-left">
    <a href="{{ route('admin.category.edit', ['category_id' => $category->id]) }}">{{ $category->name }}</a>
    @foreach($category->children as $category)
        @include('admin.pages.category.edit.category', compact('category'))
    @endforeach
</div>
