@extends('layouts.admin')

@section('content')
    @foreach($categories as $category)
        @include('admin.pages.category.edit.category', compact('category'))
    @endforeach
@endsection
