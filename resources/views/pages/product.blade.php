@extends('layouts.app')

@push('meta')
<meta name="stripe-public-key" content="{{ env('STRIPE_PUBLIC_KEY') }}">
<script type="application/ld+json">
{!! json_encode($product->generateJsonLD()) !!}
</script>
@endpush

@push('scripts')
<script src="https://js.stripe.com/v3/"></script>
@endpush

@section('content')
<div class="uk-margin-top" uk-grid>
    <div class="uk-width-1-3@l">
        <div uk-lightbox>
            @if($product->images->first())
            <a class="uk-inline" href="{{ $product->images->first()->link }}">
                <img src="{{ $product->images->first()->link }}" alt="">
            </a>
            @endif
            <div class="uk-margin-top" uk-grid>
                @foreach($product->images as $img)
                <div class="uk-width-1-3">
                    <a class="uk-inline" href="{{ $img->link }}">
                        <img src="{{ $img->link }}" alt="">
                    </a>
                </div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="uk-width-2-3@l">
        <div class="uk-card uk-card-default uk-card-body">
            <h1 class="uk-heading-divider">{{ $product->name }}</h1>
            <p class="uk-text-large">{{ $product->description }}</p>
            <div class="uk-card-footer">
                <a class="uk-button uk-button-primary uk-align-right uk-margin-remove-bottom" href="{{ route('cart.add', ['product_id' => $product->id]) }}">
                    <span uk-icon="plus" class="uk-margin-small-right"></span>
                    {{ __('order.addToCart') }}
                </a>
                <span class="uk-text-large uk-align-right">{{ Str::currency($product->price) }}</span>
            </div>
        </div>
    </div>
</div>
@endsection
