@extends('layouts.app')

@section('content')
    <div class="uk-section uk-section-small uk-flex uk-flex-center">
        <div class="uk-card uk-card-default uk-card-body uk-width-xlarge">
            @if(Auth::user()->hasPassword())
                <h2 class="uk-card-title">{{ __('user.changePassword') }}</h2>
            @else
                <h2 class="uk-card-title">{{ __('user.addPassword') }}</h2>
            @endif
            <form method="POST" action="{{ route('account.password.update') }}" class="uk-form-stacked">
                @csrf
                @if ($errors->any())
                    <div class="uk-alert-danger" uk-alert>
                        <a class="uk-alert-close" uk-close></a>
                        <p>{{ $errors->first() }}</p>
                    </div>
                @endif
                @if(Auth::user()->hasPassword())
                    <div class="uk-margin">
                        <label for="current_password" class="uk-form-label">
                            {{ __('user.currentPassword') }}
                        </label>
                        <div class="uk-form-control">
                            <input class="uk-input @error('current_password') uk-form-danger @enderror" id="current_password" name="current_password" type="password"
                                   value="{{ old('current_password') }}" required autocomplete="password" autofocus>
                            @error('current_password')
                            <span class="uk-text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                @endif

                <div class="uk-margin">
                    <label for="password" class="uk-form-label">
                        {{ __('user.newPassword') }}
                    </label>
                    <div class="uk-form-control">
                        <input class="uk-input @error('password') uk-form-danger @enderror" id="password" name="password" type="password" required>
                        @error('current-password')
                        <span class="uk-text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                </div>

                <div class="uk-margin">
                    <label for="password_confirmation" class="uk-form-label">
                        {{ __('user.confirmPassword') }}
                    </label>
                    <div class="uk-form-control">
                        <input class="uk-input @error('password_confirmation') uk-form-danger @enderror" id="password_confirmation" name="password_confirmation" type="password" required>
                        @error('password_confirmation')
                        <span class="uk-text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <div class="uk-margin">
                    <div class="uk-form-control">
                        <button type="submit" class="uk-button uk-button-primary">
                            {{ __('common.save') }}
                        </button>
                    </div>
                </div>
            </form>
            @if(!Auth::user()->hasPassword())
                <a class="uk-button uk-button-text uk-align-right uk-margin-remove-bottom" href="{{ route('account.index') }}">
                    {{ __('user.skipStep') }}
                </a>
            @endif
        </div>
    </div>
@endsection
