@extends('layouts.app')

@section('content')
    <div class="uk-section">
        @include('components.flash-messages')
        <div class="uk-flex uk-flex-center">
            <div class="uk-grid-column-small uk-width-1-1 uk-child-width-1-2@l uk-child-width-1-1@s" uk-grid>
                <div>
                    <div class="uk-card uk-card-default uk-card-body">
                        <h3 class="uk-card-title">
                            {{ __('user.account') }}
                            @if(Auth::user()->isAdmin())
                                <a class="uk-button uk-button-link uk-align-right uk-text-danger" href="{{ route('admin.index') }}">{{ __('common.administration') }}</a>
                            @endif
                        </h3>
                        <p>{{ __('common.email') }}: {{ $user->email }}<br>
                            {{ __('localization.defaultLanguage') }}: {{ trans('localization.languages.' . $user->locale) }}</p>
                        <div class="uk-button-group uk-align-right">
                            <a class="uk-button uk-button-primary" href="{{ route('account.edit') }}">{{ __('common.edit') }}</a>
                            <a class="uk-button uk-button-default" href="{{ route('account.password') }}">{{ __('user.changePassword') }}</a>
                            <a class="uk-button uk-button-default" href="{{ route('get_logout') }}">{{ __('user.logout') }}</a>
                        </div>
                    </div>
                </div>
                <div>
                    @if($address)
                        @include('components.address', ['address' => $address])
                    @else
                        <div class="uk-card uk-card-default uk-card-body">
                            <h3 class="uk-card-title">
                                {{ __('order.address') }}
                            </h3>
                            <a class="uk-button uk-button-primary" href="{{ route('account.address.add') }}">{{ __('order.addAddress') }}</a>
                        </div>

                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
