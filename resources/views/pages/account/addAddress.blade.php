@extends('layouts.app')

@section('content')
    <div class="uk-section">
        <form class="uk-form-stacked uk-container uk-container-small" method="POST" action="{{ route('account.address.saveAdd') }}">
            <fieldset class="uk-fieldset">
                <legend class="uk-legend">
                    {{ __('order.addAddress') }}
                </legend>
                <div class="uk-margin">
                    <label class="uk-form-label" for="address_name">{{ __('order.addressName') }}</label>
                    <div class="uk-form-controls">
                        <input class="uk-input uk-width-1-2" id="address_name" name="address_name" type="text" value="{{ $addressCount == 0 ? __('order.defaultAddressName') : '' }}">
                    </div>
                </div>
                @include('components.addressForm')
                <input type="submit" class="uk-button uk-button-primary" value="{{ __('common.save') }}">
            </fieldset>
        </form>

    </div>
@endsection
