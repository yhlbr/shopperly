@extends('layouts.app')

@section('content')
<div class="uk-margin-top">
    <div class="uk-card uk-card-default uk-card-body">
        <h3 class="uk-card-title">{{ __('order.cart') }}</h3>
        <table class="uk-table uk-table-divider">
            <thead>
                <tr>
                    <th>{{ trans_choice('common.product', $cart->count()) }}</th>
                    <th>{{ __('order.count') }}</th>
                    <th>{{ __('common.price') }}</th>
                    <th>{{ __('common.total') }}</th>
                </tr>
            </thead>
            <tbody>
                @foreach($cart as $idx => $p)
                <tr>
                    <td>{{ $p['product']->name }}</td>
                    <td class="uk-width-auto uk-width-1-3@l">
                        <button class="uk-icon cart-button-minus uk-visible@m" uk-icon="icon: minus" type="button"></button>
                        <input type="number" class="uk-input uk-form-width-xsmall cart-count-input" data-index="{{ $idx }}" style="text-align: center;" value="{{ $p['count'] }}">
                        <button class="uk-icon cart-button-plus uk-visible@m" uk-icon="icon: plus" type="button"></button>
                        <button class="uk-icon cart-button-delete uk-text-danger uk-margin-left" uk-icon="icon: trash" type="button"></button>
                    </td>
                    <td>{{ Str::currency($p['product']->price) }}</td>
                    <td>{{ Str::currency($p['product']->price * $p['count']) }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <a href="{{ route('order.checkout') }}" class="uk-button uk-button-primary uk-align-right">{{ __('order.checkout') }}</a>
        <span class="uk-text-large uk-align-right">{{ Str::currency($total) }}</span>
    </div>
</div>
@endsection

@push('scripts')
<script>
    var updateValue = debounce(function(index, value) {
        $.get('{{ url("/cart/modify") }}/' + index + '/' + value, function() {
            window.location.reload();
        });
    }, 400);

    $('.cart-button-minus').click(function() {
        var input = $(this).parent().find('input');
        var index = input.data('index');
        var value = parseInt(input.val()) - 1;
        if (value == 0) return;
        input.val(value);
        updateValue(index, value);
    });
    $('.cart-button-plus').click(function() {
        var input = $(this).parent().find('input');
        var index = input.data('index');
        var value = parseInt(input.val()) + 1;
        input.val(value);
        updateValue(index, value);
    });

    $('.cart-count-input').change(function() {
        var index = $(this).data('index');
        var value = parseInt($(this).val());
        updateValue(index, value);
    });

    $('.cart-button-delete').click(function() {
        var index = $(this).parent().find('input').data('index');
        updateValue(index, 0);
        var row = $(this).parents('tr');
        row.remove();
    });


    function debounce(a, b, c) {
        var d;
        return function() {
            var e = this,
                f = arguments;
            clearTimeout(d), d = setTimeout(function() {
                d = null, c || a.apply(e, f)
            }, b), c && !d && a.apply(e, f)
        }
    }
</script>
@endpush
