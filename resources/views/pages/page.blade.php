@extends('layouts.app')

@push('styles')
    <link rel="stylesheet" href="{{ route('page.styles', ['page' => $page->id]) }}"/>
@endpush

@section('content')
    <div id="page-content">
        {!! $page->{'gjs-html'} !!}
    </div>
@endsection
