@extends('layouts.app')

@section('content')
@include('components.flash-messages')
<div class="uk-child-width-1-2@s" uk-grid="masonry: true">
@foreach($products as $product)
    @include('components.productCard', $product)
@endforeach
</div>
@endsection
