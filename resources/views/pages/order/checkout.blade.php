@extends('layouts.app')

@section('content')
<div class="uk-margin-top">
    <form class="uk-form-stacked uk-container uk-container-small" method="POST" action="{{ route('order.saveCheckout') }}">
        @csrf
        <fieldset class="uk-fieldset">
            <legend class="uk-legend">
                {{ __('order.address') }}
                @include('components.addressSwitcher', ['addAddressRoute' => route('order.switchAddress', ['address_id' => 0])])
            </legend>
            @if ($errors->any())
            <div class="uk-alert-danger" uk-alert>
                <a class="uk-alert-close" uk-close></a>
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            @if($addressId == 0)
            <div class="uk-margin">
                <label class="uk-form-label" for="address_name">{{ __('order.addressName') }}</label>
                <div class="uk-form-controls">
                    <input class="uk-input uk-width-1-2" id="address_name" name="address_name" type="text" value="{{ old('name') ?? $address->name ?? '' }}">
                </div>
            </div>

            @endif
            @include('components.addressForm')
            <input type="submit" class="uk-button uk-button-primary uk-align-right" value="Continue">
        </fieldset>
    </form>
</div>
@endsection
