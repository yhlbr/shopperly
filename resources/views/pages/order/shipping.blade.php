@extends('layouts.app')

@section('content')
<div class="uk-margin-top">
    <form class="uk-form-stacked uk-container uk-container-small" method="POST" action="{{ route('order.saveCheckout') }}">
        @csrf
        <fieldset class="uk-fieldset">
            <legend class="uk-legend">
                {{ __('order.shipping') }}
            </legend>
            @foreach($shippingOptions as $option)
            <div class="uk-margin uk-card uk-card-body uk-card-default uk-card-hover uk-card-small">
                <h3 class="uk-card-title">{{ $option->name }}</h3>
                <p>{{ $option->details }}</p>
                <a href="{{ route('order.saveShipping', ['option_id' => $option->id]) }}" class="uk-button uk-button-primary uk-align-right uk-margin-small uk-margin-small-left">{{ __('common.choose') }}</a>
                <span class="uk-text-large uk-align-right uk-margin-small">{{ Str::currency($option->price) }}</span>
            </div>
            @endforeach
        </fieldset>
    </form>
</div>
@endsection
