@extends('layouts.app')

@section('content')
<div class="uk-margin-top">
    <h1>{{ __('order.success') }}</h1>
    <p>{{ __('order.successMessage') }}</p>
</div>
@endsection
