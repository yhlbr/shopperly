@extends('layouts.app')

@section('content')
<div class="uk-margin-top">
    <form id="paymentForm" class="uk-form-stacked uk-container uk-container-small" method="POST" action="{{ route('order.complete') }}">
        @csrf
        <fieldset class="uk-fieldset">
            <legend class="uk-legend uk-text-secondary">
                {{ __('order.confirmation') }}
            </legend>
            @include('components.flash-messages')
            <div class="uk-margin uk-card uk-card-body uk-card-default uk-card-hover uk-card-small">
                <h3 class="uk-card-title">{{ ucfirst(trans_choice('common.product', $cart->count())) }}</h3>
                <table class="uk-table uk-table-divider">
                    <thead>
                        <tr>
                            <th>{{ trans_choice('common.product', $cart->count()) }}</th>
                            <th>{{ __('order.count') }}</th>
                            <th>{{ __('common.price') }}</th>
                            <th>{{ __('common.total') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($cart as $idx => $p)
                        <tr>
                            <td>{{ $p['product']->name }}</td>
                            <td>{{ $p['count'] }}</td>
                            <td>{{ Str::currency($p['product']->price) }}</td>
                            <td>{{ Str::currency($p['product']->price * $p['count']) }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="uk-margin uk-card uk-card-body uk-card-default uk-card-hover uk-card-small">
                <h3 class="uk-card-title">{{ $shipping->name }}</h3>
                <p>{{ $shipping->details }}</p>
                <span class="uk-text-secondary uk-margin-small">{{ Str::currency($shipping->price) }}</span>
            </div>
            <div class="uk-margin uk-card uk-card-body uk-card-default uk-card-hover uk-card-small">
                <h3 class="uk-card-title">Payment</h3>
                <form class="uk-form-stacked uk-container uk-container-small" method="POST" action="{{ route('order.confirmation') }}">
                    @csrf
                    <fieldset class="uk-fieldset">
                        <div class="uk-margin">
                            <label class="uk-form-label" for="card_number">Card No.</label>
                            <div class="uk-form-controls">
                                <div id="card_number" class="field"></div>
                            </div>
                        </div>
                        <div class="uk-margin" uk-grid>
                            <div class="uk-width-1-4">
                                <label class="uk-form-label" for="card_expiry">Expiry Date</label>
                                <div class="uk-form-controls">
                                    <div id="card_expiry" class="field"></div>
                                </div>
                            </div>

                            <div class="uk-width-1-4">
                                <label class="uk-form-label" for="card_number">CVC Code</label>
                                <div class="uk-form-controls">
                                    <div id="card_cvc" class="field"></div>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </form>
                <button id="payButton" data-secret="{{ $intent->client_secret }}" class="uk-button uk-button-primary uk-align-right uk-margin-small uk-margin-small-left">{{ __('order.complete') }}</button>
                <span class="uk-text-large uk-align-right uk-margin-small">{{Str::currency($total)}}</span>
                <div id="cardErrorsAlert" style="display: none;" class="uk-alert-danger" uk-alert>
                    <a class="uk-alert-close" uk-close></a>
                    <p id="cardErrors"></p>
                </div>

            </div>
        </fieldset>
    </form>
</div>
@endsection

@push('scripts')
<script src="https://js.stripe.com/v3/"></script>
<script>
    // Create an instance of the Stripe object
    var stripe = Stripe('{{ env("STRIPE_PUBLIC_KEY") }}');

    // Create an instance of elements
    var elements = stripe.elements();

    var style = {
        base: {
            fontWeight: 400,
            fontSize: '16px',
            lineHeight: '1.4',
            color: '#555',
        },
        invalid: {
            color: '#eb1c26',
        }
    };

    var cardElement = elements.create('cardNumber', {
        style: style
    });
    cardElement.mount('#card_number');

    var exp = elements.create('cardExpiry', {
        'style': style
    });
    exp.mount('#card_expiry');

    var cvc = elements.create('cardCvc', {
        'style': style
    });
    cvc.mount('#card_cvc');

    cardElement.on('change', function(event) {
        var displayError = $('#cardErrors');
        if (event.error) {
            displayError.text(event.error.message);
            $('#cardErrorsAlert').css('display', 'inherit');
        } else {
            displayError.text('');
            $('#cardErrorsAlert').css('display', 'none');
        }
    });

    // Get payment form element
    var form = $('#paymentForm');

    form.on('submit', function(ev) {
        if (document.getElementsByName('intentId').length > 0) {
            return true;
        } else {
            ev.preventDefault();
        }

        $('#payButton').prepend('<div uk-spinner="ratio: 0.8" class="uk-margin-right"></div>');
        $('#payButton div').prop('disabled', true);

        stripe.confirmCardPayment($('#payButton').data('secret'), {
            payment_method: {
                card: cardElement,
                billing_details: {}
            }
        }).then(function(result) {
            if (result.error) {
                $('#cardErrors').text(result.error.message);
                $('#cardErrorsAlert').css('display', 'inherit');
                $('#payButton div').remove();
                $('#payButton div').prop('disabled', false);
            } else {
                // The payment has been processed!
                if (result.paymentIntent.status === 'succeeded') {
                    var hiddenInput = document.createElement('input');
                    hiddenInput.setAttribute('type', 'hidden');
                    hiddenInput.setAttribute('name', 'intentId');
                    hiddenInput.setAttribute('value', result.paymentIntent.id);
                    form.append(hiddenInput);

                    form.submit();
                }
            }
        });
    });
</script>
@endpush
