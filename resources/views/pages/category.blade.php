@extends('layouts.app')

@section('content')
<div class="uk-child-width-1-2@s uk-margin-top" uk-grid="masonry: true">
@foreach($products as $product)
    @include('components.productCard', $product)
@endforeach
</div>
@endsection