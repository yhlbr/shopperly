<!DOCTYPE html>
<html lang="{{ \App\Locale::getLocale() }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @stack('meta')
    <link rel="icon" href="{{ URL::asset('/favicon.png') }}" type="image/x-icon"/>
    <title>{{ config('settings.ShopName') }}</title>
    @vite('resources/sass/app.scss')
    @stack('styles')
</head>
<body>
    @yield('body')
    @vite('resources/js/app.js')
    @stack('scripts')
</body>
</html>
