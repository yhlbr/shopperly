@extends('layouts.app')

@section('content')
    <div class="uk-section uk-section-small uk-flex uk-flex-center">
        <div class="uk-card uk-card-default uk-card-body uk-width-large">
            <h2 class="uk-card-title">{{ __('user.resetPassword') }}</h2>
            <form method="POST" action="{{ route('password.email') }}" class="uk-form-stacked">
                @csrf
                @if (session('status'))
                    <div class="uk-alert-danger" uk-alert>
                        <a class="uk-alert-close" uk-close></a>
                        <p>{{ session('status') }}</p>
                    </div>
                @endif

                <div class="uk-margin">
                    <label for="email" class="uk-form-label">
                        {{ __('common.email') }}
                    </label>
                    <div class="uk-form-control">
                        <input class="uk-input @error('email') uk-form-danger @enderror" id="email" name="email" type="email"
                               value="{{ old('email') }}" required autocomplete="email" autofocus>
                        @error('email')
                        <span class="uk-text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <div class="uk-margin">
                    <div class="uk-form-control">
                        <button type="submit" class="uk-button uk-button-primary">
                            {{ __('user.sendPasswordReset') }}
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
