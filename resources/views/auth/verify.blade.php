@extends('layouts.app')

@section('content')
    <div class="uk-section uk-section-small uk-flex uk-flex-center">
        <div class="uk-card uk-card-default uk-card-body uk-width-xlarge">
            <h2 class="uk-card-title">{{ __('user.verifyEmail') }}</h2>
            @if (session('resent'))
                <div class="uk-alert-success" uk-alert>
                    {{ __('user.newLinkSent') }}
                </div>
            @endif

            <p>
                {{ __('user.checkForVerificationLink') }}<br/>
                {{ __('user.requestAnotherLink') }}
            </p>
            <form method="POST" action="{{ route('verification.resend') }}">
                @csrf
                <button type="submit"
                        class="uk-button uk-button-primary">{{ __('user.requestNewLink') }}</button>
            </form>
        </div>
    </div>
@endsection
