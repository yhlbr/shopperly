@extends('layouts.app')

@section('content')
    <div class="uk-section uk-section-small uk-flex uk-flex-center">
        <div class="uk-card uk-card-default uk-card-body uk-width-large">
            <h2 class="uk-card-title">{{ __('user.login') }}</h2>
            @include('components.flash-messages')
            <form method="POST" action="{{ route('login') }}" class="uk-form-stacked">
                @csrf
                <div class="uk-margin">
                    <label for="email" class="uk-form-label">
                        {{ __('common.email') }}
                    </label>
                    <div class="uk-form-control">
                        <input class="uk-input @error('email') uk-form-danger @enderror" name="email" id="email" type="email"
                               value="{{ old('email') }}" required autocomplete="email" autofocus>
                        @error('email')
                        <span class="uk-text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <div class="uk-margin">
                    <label for="password" class="uk-form-label">
                        {{ __('common.password') }}
                    </label>
                    <div class="uk-form-control">
                        <input id="password" type="password"
                               class="uk-input @error('password') uk-form-danger @enderror" name="password" required
                               autocomplete="current-password">
                        @error('password')
                        <span class="uk-text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <div class="uk-margin">
                    <div class="uk-form-control">
                        <input class="uk-checkbox" type="checkbox" name="remember"
                               id="remember" checked>
                        <label for="remember">
                            {{ __('user.rememberMe') }}
                        </label>
                    </div>
                </div>
                <div class="uk-margin">
                    <div class="uk-form-control">
                        <button type="submit" class="uk-button uk-button-primary">
                            {{ __('user.login') }}
                        </button>

                        @if (Route::has('password.request'))
                            <a class="uk-button uk-button-link uk-margin-left" href="{{ route('password.request') }}">
                                {{ __('user.forgotPassword') }}
                            </a>
                        @endif
                    </div>
                </div>
            </form>
            <a class="uk-button uk-button-secondary uk-button-large uk-width-1-1 uk-margin-small-bottom" href="{{ route('login_oauth', ['provider' => 'google']) }}">{{ __('user.loginWith', ['provider' => 'Google']) }}</a>
            <a class="uk-button uk-button-secondary uk-button-large uk-width-1-1" href="{{ route('register') }}">{{ __('user.register') }}</a>
        </div>
    </div>
@endsection
