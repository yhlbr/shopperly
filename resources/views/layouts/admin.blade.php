@extends('master')

@push('scripts')
    @routes
    @vite('resources/js/admin/admin.js')
@endpush

@section('body')
<main>
    @include('admin.components.topbar')
    <div class="uk-container uk-container-large">
        <div class="" uk-grid>
            <div id="sidenav-container" class="uk-width-1-5@l uk-width-1-1@s">
                @include('admin.components.sidenav')
            </div>
            <div id="page-container" class="uk-width-4-5@l uk-width-1-1@s">
                @if(Session::has('alert-message') || Session::has('alert-html'))
                    @include('admin.components.flashAlert')
                @endif
                @yield('content')
            </div>
        </div>
    </div>
</main>
@endsection
