@extends('master')

@section('body')
    <div class="page-container">
        <main>
            @include('components.nav')
            <div class="uk-container uk-margin-top uk-margin-bottom page-content-container">
                @yield('content')
            </div>
        </main>
        <footer class="uk-card uk-card-body uk-card-default uk-background-secondary">
            <div class="uk-grid">
                <div class="uk-width-1-4">
                </div>
                <div class="uk-width-1-2">
                    <p class="uk-text-large">Contact Us!</p>
                    <a href="mailto:{{ config('settings.Email') }}">{{ config('settings.Email') }}</a>
                </div>
            </div>
        </footer>
    </div>
@endsection
