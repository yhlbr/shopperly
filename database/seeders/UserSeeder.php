<?php

namespace Database\Seeders;

use App\User;
use App\UserAddress;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Create 3 records of customers
        User::factory()->count(3)->create()->each(function ($user) {
            // Seed the relation with one address
            $address = UserAddress::factory()->make();
            $user->addresses()->save($address);
        });
    }
}
