<?php

namespace Database\Seeders;

use App\Category;
use App\Translations\CategoryLocale;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Create 2 records of categories
        Category::factory()->count(2)->create()->each(function ($category) {
            // Seed the relation with one locale
            $translation = CategoryLocale::factory()->make();
            $category->translations()->save($translation);

            // Add subcategories
            Category::factory()->count(2)->create(['parent_id' => $category->id])->each(function ($category) {
                // Seed the relation with one locale
                $translation = CategoryLocale::factory()->make();
                $category->translations()->save($translation);
            });
        });
    }
}
