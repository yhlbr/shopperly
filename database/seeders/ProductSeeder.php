<?php

namespace Database\Seeders;

use App\Category;
use App\CategoryProduct;
use App\Product;
use App\Translations\ProductLocale;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Create 10 records of products
        Product::factory()->count(10)->create()->each(function ($product) {
            // Seed the relation with one locale
            $translation = ProductLocale::factory()->make();
            $product->translations()->save($translation);

            // Add to categories
            $categories = Category::inRandomOrder()->limit(2)->get();
            foreach ($categories as $category) {
                $c_p = new CategoryProduct;
                $c_p->product_id = $product->id;
                $c_p->category_id = $category->id;
                $c_p->save();
            }
        });
    }
}
