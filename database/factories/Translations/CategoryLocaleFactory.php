<?php

namespace Database\Factories\Translations;

use App\Translations\CategoryLocale;
use Illuminate\Database\Eloquent\Factories\Factory;

class CategoryLocaleFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = CategoryLocale::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $language = $this->faker->randomElement(['de', 'en']);
        return [
            'locale' => $language,
            'name' => $this->getRandomTitle($language),
            'description' => $this->faker->paragraph
        ];
    }

    public function getRandomTitle($language = 'de') {
        switch($language) {
            case 'de':
                $array = [
                    'Kleidung',
                    'Lebensmittel',
                    'Getränke',
                    'Instrumente',
                    'Filme',
                    'Sport',
                    'Werkzeuge',
                    'Musik',
                    'Küche',
                    'Uhren',
                    'Spiele',
                ];
                break;
            case 'en':
            default:
                $array = [
                    'Clothes',
                    'Food',
                    'Beverages',
                    'Instruments',
                    'Movies',
                    'Sports',
                    'Tools',
                    'Music',
                    'Kitchen',
                    'Watches',
                    'Games'
                ];
                break;
        }
        return $this->faker->randomElement($array);
    }
}
