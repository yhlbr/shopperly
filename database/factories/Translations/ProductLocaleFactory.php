<?php

namespace Database\Factories\Translations;

use App\Translations\ProductLocale;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductLocaleFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ProductLocale::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $language = $this->faker->randomElement(['de', 'en']);
        return [
            'locale' => $language,
            'name' => $this->getRandomTitle($language),
            'description' => $this->faker->paragraph
        ];
    }

    public function getRandomTitle($language = 'de') {
        switch($language) {
            case 'de':
                $array = [
                    'Die Bestie',
                    'Was wir wollten',
                    'All die verdammt perfekten Tage',
                    'Verirrte Kugel',
                    'Betonrausch',
                    'Banden von Marseille'
                ];
                break;
            case 'en':
            default:
                $array = [
                    'Cloud Atlas',
                    'Blood Diamond',
                    'Tenet',
                    'Paradise Z',
                    'Die Bestie',
                    'The Midnight Sky',
                    'Follow Me',
                    'Was wir wollten',
                    'Legacy of Lies',
                    'Downhill',
                    'Greenland',
                    'Tesla',
                    'Fantasy Island'
                ];
                break;
        }
        return $this->faker->randomElement($array);
    }
}
