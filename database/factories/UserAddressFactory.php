<?php

namespace Database\Factories;

use App\UserAddress;
use Illuminate\Database\Eloquent\Factories\Factory;

class UserAddressFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = UserAddress::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $faker = $this->faker;
        return [
            'address_name' => $faker->company,
            'firstname' => $faker->firstName,
            'lastname' => $faker->lastName,
            'company' => $faker->company,
            'street' => $faker->streetName,
            'street_no' => $faker->buildingNumber,
            'city' => $faker->city,
            'postcode' => $faker->postcode,
            'country' => $faker->country
        ];
    }
}
