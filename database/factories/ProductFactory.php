<?php

namespace Database\Factories;

use App\Product;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $faker = $this->faker;
        $product_no = $faker->bothify('???######');
        return [
            'product_nr' => $product_no,
            'visible' => true,
            'price' => $faker->randomFloat(2, 1, 200),
            'weight' => $faker->randomNumber(3),
            'seo_name' => $product_no
        ];
    }
}
