<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Setting;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->string('key')->unique();
            $table->primary('key');
            $table->string('value');
        });
        Setting::create([ 'key' => 'ShopName', 'value' => config('app.name') ]);
        Setting::create([ 'key' => 'Email', 'value' => 'hello@shopperly.test' ]);
        Setting::create([ 'key' => 'Languages', 'value' => 'de,en' ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
